<?php 
ini_set('date.timezone','Asia/Shanghai');
//error_reporting(E_ERROR);
require_once "../lib/WxPay.Api.php";
require_once "WxPay.JsApiPay.php";

//打印输出数组信息
function printf_info($data)
{
    foreach($data as $key=>$value){
        echo "<font color='#00ff55;'>$key</font> : $value <br/>";
    }
}

//1.公众号设置-》功能设置-》网页授权域名中添加微信支付服务器域名(不支持带端口号)
//2.微信支付-》开发配置-》公众号支付-》支付授权目录中添加jsapi所在地址
//服务器配置时指向Wxpay这个文件目录即可
//微信支付服务器地址*******配置项
$wxurl = 'http://weshopphp.chihuoyun.com';

// ①、获取用户openid
$tools = new JsApiPay();
$openId = $tools->GetOpenid();
echo $openId;

$notifyurl = $wxurl."/example/notify.php";

if(isset($_GET['fee'])&&isset($_GET['orderid'])&&isset($_GET['zorderid'])&&isset($_GET['nid'])) {
	$fee = $_GET['fee'];
    // 测试环境设置金额为1
    // $testfee = 1;
	$orderid = $_GET['orderid'];
	$regorderid = $_GET['zorderid'];
	$nid = $_GET['nid'];
	//②、统一下单
	$input = new WxPayUnifiedOrder();
	$input->SetBody($orderid);
	$input->SetOut_trade_no(WxPayConfig::MCHID.date("YmdHis"));//下单订单号
	$input->SetTotal_fee($fee);//下单金额，单位：分
	$input->SetTime_start(date("YmdHis"));
	$input->SetTime_expire(date("YmdHis", time() + 600));
	$input->SetGoods_tag("");
	$input->SetNotify_url($notifyurl);
	$input->SetTrade_type("JSAPI");
	$input->SetOpenid($openId);
	$order = WxPayApi::unifiedOrder($input);
	$jsApiParameters = $tools->GetJsApiParameters($order);
	echo $jsApiParameters;

	//获取共享收货地址js函数参数
	$editAddress = $tools->GetEditAddressParameters();
}else{
	$fee = '';
	$orderid = '';
	echo"<script>alert('非法调用');history.go(-1);</script>";  
	exit;
}

//③、在支持成功回调通知中处理成功之后的事宜，见 notify.php
/**
 * 注意：
 * 1、当你的回调地址不可访问的时候，回调通知会失败，可以通过查询订单来确认支付是否成功
 * 2、jsapi支付时需要填入用户openid，WxPay.JsApiPay.php中有获取openid流程 （文档可以参考微信公众平台“网页授权接口”，
 * 参考http://mp.weixin.qq.com/wiki/17/c0f37d5704f0b64713d5d2c37b468d75.html）
 */

?>

<html>
<head>
    <meta http-equiv="content-type" content="text/html;charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/> 
    <title>微信支付</title>
    <script type="text/javascript">
    //通知的地址    服务器地址
    var url = 'http://weshopphp.chihuoyun.com/example/';
    var state = '';
    var regorderid = "<?php echo $regorderid;?>";

    function PostState(url,state)
    {
		var xhr = new XMLHttpRequest();
		xhr.open('POST', url, true);
		xhr.setRequestHeader( 'Content-Type', 'application/json' );
		var fee = "<?php echo $fee;?>";
		var nid = "<?php echo $nid;?>";
		//添加微信支付完成时间
		var timestamp = (new Date()).valueOf();
		var newtimestampint = timestamp.toString();
		var newtimestampstr = newtimestampint.substring(0, newtimestampint.length-3);
		var msg = {"regorderid": regorderid, "fee": fee, "state": state, "time": newtimestampstr, "nid": nid};
		xhr.send(JSON.stringify(msg));
		var state = state;
	    xhr.onerror = function(event) {
	         alert("on error");
	         alert(event);
	    }
      xhr.onreadystatechange = function() {
        	if(xhr.readyState == 4 && xhr.status == 200){ 
        		if(state === 'ok'){
        			// 成功
        			// window.location.href = 'http://weshopphp.chihuoyun.com/result?uuid='+regorderid;
        		}else{
        			// 失败
        			// window.location.href = 'http://weshopphp.chihuoyun.com/pay';
        		}	
          }
      };
    }

	//调用微信JS api 支付
	function jsApiCall()
	{
		WeixinJSBridge.invoke(
			'getBrandWCPayRequest',
			<?php echo $jsApiParameters; ?>,
			function(res) {
            switch(res.err_msg) {
                case 'get_brand_wcpay_request:cancel':
                    if(PostState(url,'cancle')){
                    	// window.location.href = 'https://weshop.chihuoyun.com/pay';
                	}
                    break;
                case 'get_brand_wcpay_request:fail':
                    if(PostState(url,'fail')){
                    	// window.location.href = 'https://weshop.chihuoyun.com/pay';
                	}
                    break;
                case 'get_brand_wcpay_request:ok':
                    if(PostState(url,'ok')){
                    	// window.location.href = 'https://weshop.chihuoyun.com/result?uuid='+regorderid;
                	}
                    break;
                default:
                    alert(JSON.stringify(res));
                    break;
            }
        }

		);
	}

	function callpay()
	{
		if (typeof WeixinJSBridge == "undefined"){
		    if( document.addEventListener ){
		        document.addEventListener('WeixinJSBridgeReady', jsApiCall, false);
		    }else if (document.attachEvent){
		        document.attachEvent('WeixinJSBridgeReady', jsApiCall); 
		        document.attachEvent('onWeixinJSBridgeReady', jsApiCall);
		    }
		}else{
		    jsApiCall();
		}
	}

	</script>
</head>
<body>
    </br></br></br></br>
    <div align="center">
        <button style="width:210px; height:30px; background-color:#FE6714; border:0px #FE6714 solid; cursor: pointer;  color:white;  font-size:16px;" type="button" onclick="callpay()" >点击支付</button>
    </div>
</body>
</html>